# Shared Docker

Public shared docker images.
Several images that can be reused across projects of the team.
They are published in the container registry of this project, so they do not fall in the dockerhub pull limitation.